# UI Architecture

Each Angular application is a single-page app, known as SPA with the following structure:

1.  The main entry point of the application.
2.  The shell or app-shell, which top-level app logic, router, and so on. It should contain the minimal HTML, CSS and JavaScript required to power the skeleton of your UI and the core components necessary to get your app off the ground, such as navigation.
3.  Lazily loaded fragments of the app. A fragment can represents the code of a particular view.

![UI App Architecture](https://developers.google.com/web/fundamentals/performance/prpl-pattern/images/app-build-components.png)

![UI App Architecture example](https://developers.google.com/web/fundamentals/architecture/images/appshell.png)

## How is this architecture realized?

Since Angular application is composed of a tree of components, we start by properly define and modularized these components in the following manner:

1.  Core Module
2.  Shared Module
3.  Feature Module

### Core Module

Core module should only contain single-use classes to be used application-wide, and its main role is to orchestrate the application. App-shell is basically the app-shell.

Some of the common classes to be included in Core Module are:

1.  Singleton service whose instance will be shared throughout the application (e.g. Authentication/Authorization/User service)
2.  Single-use components, which only need to import it once when application starts, and nowhere else (e.g Navigation or Spinner)

Avoid doing the following:

1.  Never import Core Module anywhere except in the App Module

### Shared Module

Shared module should contain components, directives and pipes which will be re-used and referenced by components declared in other feature modules. Shared Module exists to make commonly used components, directives and pipes available for use in the templates of components in other modules.

Some of the common classes to be included in Shared Module are:

- Number/Date format directives
- List Search component

Avoid doing the following:

1.  Avoid providing services in shared modules. Services are usually singletons and that are provided once for the entire application or in a particular feature module.

Since Shared module is meant to be shared across modules, or even across multiple applications, **unit test case and documentation is very crucial.**

### Feature Module

Feature modules are essentially fragments of the entire application which are lazily loaded as needed. Each feature module should be grouped under a business domain folder. A feature within a business domain is usually determined by the Information Architecture, also known as site map.

Feature module should only contain feature related classes and components within a business domain. Each feature should have its own folder, module, and at least one root/page level component, which can be made up of other components. Each feature should have its own router to handle URL routing to different components.

Each feature should also have a shared folder containing classes that are meant to be re-used within the feature.

1.  Services
2.  Model
3.  Directives/pipes

### Components Composition/Architecture

Now that we have defined a meaningful way to modularized our code, what's next? As mentioned before, Angular application is essentially a tree of components that consumes Services/Models/Directives/Pipes.

![Component Tree](https://angular.io/generated/images/guide/router/component-tree.png)

An extremely important aspect to building scalable and enterprise-level Angular architecture is to properly format, segregate, and identify areas of your application in a modular mindset. Although a page view can be written as one gigantic component, there are often much better ways of doing so.

Every new business domain will be an Angular module. For each business domain, there will be shared utilities such as services, data stores, routing, class definitions, directives, and pipes. It is normal and expected to have multiple page views for each business domain.

In order to create a page, it is important to do a quick sketch of how the view should be broken down into separate components for reusability and modularity.

The flow is as follows:

```
shared routing module --> container component (page-level) --> smart components --> presentational components
```

In general, the _container component_ should only be a shell to handle routing and contain the different smart components. The container component shall contain **only** smart components. If state is required to manage data in the page and it is related to routing, use dependency injection for the required state service.

_Smart components_ are considered specific to the application and will have some data manipulation involved. Other than manipulating the data from the required services to the presentational components, all other business logic should be kept in services for isolation and reuse.

_Dumb components_ are at the lowest level of the chain and will generally often be UX patterns and/or Angular Material HTML elements. These will be used naturally in the template of the smart components. Never inject data via dependency injection into dumb components, as this convulses the separation of concerns/responsibilities. Only receive data through `@Input` and `@Output` decorators.

Note: For Dumb Components, we will be distributing via internal component library, [CGI Sentry Angular Components](http://w360bitbucket.mdc.cginet:7990/projects/W360TP/repos/cgi-sentry-angular-components-lib/browse). This library is based on Angular Material. The purpose of this library is to provide CGI themed components, as well as reusable patterns across application. Please check out this repository for more information.

#### README.MD

Each component should have its own README.md that provide more comprehensive documentation if necessary. For example, if the component is reusable across application (Atom or Oragnism type component), a more detailed documentation should outline the purpose of this component, how it should be used, screenshots, and different example of usage. Similar to [this](https://compodoc.github.io/compodoc-demo-todomvc-angular/components/TodoComponent.html#readme).

### Model

Instead of managing a bunch of class members/attributes within a component, create entity class model that can be instantiated to represent the data object. An example:

```
export class Hero {
  id: number;
  name: string;
}
```

### Services

Services are often used to retrieve and process data from multiple sources, be it via REST or even internal state management libraries like Redux, Flux, or @ngrx/store. In our Wealth applications, the most common use cases are as follows:

- to handle REST calls
- to manipulate data for business reasons and applying business logic
- to store state within a single module

A suggested way of using DI within smart components is to create a wrapper [_sandbox_](https://blog.strongbrew.io/A-scalable-angular2-architecture/) service that abstracts away the functionality of the services. It's essentially a hub for all associated services and so, a smart component need only inject this one service instead of multiple. This can help to clean and clarify the dependency injection in various applications by way of simpler smart components.
![Sandbox Service](http://w360app1.mdc.cginet:8081/assets/sandbox_service.png)

### Library
In addition to create application project with Angular CLI, you now can also create an library application. So when do you create an application or library project?
- Application project usually meant to deliver specific features and functionalities to specific set of of users. Most of the components built in the application are only meant to be used within the application, and are not meant to be shared outside of the application.
- Library project usually meant to deliver reusable features and functionalities to a broader set of users. Most of the components built in the library are meant to be shared and used across applications. For instance, our internal component library, [CGI Sentry Angular Components](http://w360bitbucket.mdc.cginet:7990/projects/W360TP/repos/cgi-sentry-angular-components-lib/browse). The purpose of this library is to provide CGI themed components, as well as reusable patterns across application.
![Angular Library Architecture](http://w360app1.mdc.cginet:8081/assets/angular_lib_arch.png)

For more information, check out the [Angular ClI guide](https://github.com/angular/angular-cli/wiki/stories-create-library)

---

## Best Practices/Success Criteria

Before pushing any code to the repository, make sure to complete the following:

### 1. Naming Convention

Please follow the file naming convention as outlined in the [Angular guidelines](https://angular.io/guide/styleguide#naming). Please make sure we add a prefix **"cgi"** to all directives/components.

For SCSS/SASS class definitions names, please make use of [BEM](https://en.bem.info/methodology/naming-convention/) naming.

### 2. Module

Each feature should have its own folder, module, and at least one root/page level component, which can be made up of other components. Each feature should have its own router to handle URL routing to different components.

Each feature should also have a shared folder containing classes that are meant to be re-used within the feature.

1.  Services
2.  Model
3.  Directives/pipes

### 3. Component

Component folder should contain one component at a time, which are made of the following files:

1.  HTML
2.  SCSS
3.  Spec.ts (Unit test)
4.  Typescript
5.  README.md

#### SCSS

1.  For SCSS/SASS class definitions names, please make use of [BEM](https://en.bem.info/methodology/naming-convention/) naming.
2.  Use variables! Think of variables as a way to store information that you want to reuse throughout your stylesheet. You can store things like colors, font stacks, or any CSS value you think you'll want to reuse.
3.  Use nesting! Provide clear visual hierarchy of your style classes! Also allows your CSS changes to be localized; not applied globally by accident. Be aware that overly nested rules will result in over-qualified CSS that could prove hard to maintain and is generally considered bad practice. As a rule of thumb, never nest more than 3 levels.
4.  Use of mixins/inheritance for reusability!
5.  Avoid using !important in your CSS styles

#### Spec.ts (Unit test)

Ensure to write unit test cases for each component in Angular application, **especially components/services/pipes that are reusable and shared across the application**. You must automate and unit-test the interface of the classes.

Reference:

1.  [Walk-through of basic Karma/Jasmine Testing principles](https://medium.com/google-developer-experts/angular-2-testing-guide-a485b6cb1ef0)
2.  [Testing examples](http://blog.rangle.io/testing-angular-2-applications/)
3.  [Angular best practices](https://angular.io/guide/testing#test-a-component)
4.  [Consuming a REST API call in testing](https://offering.solutions/blog/articles/2016/02/01/consuming-a-rest-api-with-angular-http-service-in-typescript/)

#### Typescript

File that actually contains logic for your component. Make sure you properly document the component, as well as its members and functions with comment block, so that other developers can easily understand the features of your component.

An example of proper comment block:

```
/**
 * @param {string} target  The target to process see {@link Todo}
 *
 * @example
 * This is a good example
 * processTarget('yo')
 *
 * @returns      The processed target number
 */
function processTarget(target:string):number;
```

[Compodocs](https://compodoc.github.io/) will also scan through these comment blocks, and generate a documentation site similar to [this](https://compodoc.github.io/compodoc-demo-todomvc-angular/components/TodoComponent.html)

[Compodocs](https://compodoc.github.io/) will flag components that lack documentation. Please make sure you achieve 100% of documentation coverage.

![Compodoc Documentation Coverage](https://compodoc.github.io/website/assets/img/screenshots/8.png)

Please refer to the [Compodoc Guides](https://compodoc.github.io/website/guides/comments.html) for more info.

Please lint your code to check for any errors and violations:

1.  Run `ng lint` to check for formatting errors and best practices.
2.  Use [Visual Studio Code](https://code.visualstudio.com/) to check for any errors

### 4. Accessibility

Run the [Lighthouse](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk) and [Axe](https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd?utm_source=chrome-ntp-icon) on your new components/pages, and make sure you achieve 100% score on accessibility.

---

## Git

Here are some useful links that you can read about Git if you are unfamiliar with Git:

- http://w360confluence.mdc.cginet:8090/display/AR/Git+Basics
- http://w360confluence.mdc.cginet:8090/display/AR/Workflows

### Git Workflow

We will be mainly adopting Gitflow as our main workflow for code commit. The diagram below outlines high level workflow model.
![Gitflow](https://wac-cdn.atlassian.com/dam/jcr:21cf772d-2ba5-4686-8259-fcd6fd2311df/05.svg?cdnVersion=dj)

You can find out more details about workflow model and the purpose of each branch in the workflow via [Confluence](http://w360confluence.mdc.cginet:8090/display/AR/Git+Architecture+and+Naming+Convention).

### Git Contribution Guide

For each git commit comments, we will be following the standards outline below:

Commit comment's must include <Type>: JIRA Story / Task / Bug Number (Not Sub-task Number) and brief description of the changes (not story details). This allows us to look holistically at all commits in JIRA / other tools across various code repositories and skill-sets (Java, UI, DDL, SQL, Reporting)

Commit types consists of:
- **feat**: A new feature
- **fix**: A bug fix
- **docs**: Documentation only changes
- **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing
  semi-colons, etc)
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **test**: Adding missing tests or correcting existing tests
- **build**: Changes that affect the build system, CI configuration or external dependencies
  (example scopes: gulp, broccoli, npm)
- **chore**: Other changes that don't modify `src` or `test` files

***Note: for more details, please refer to [Confluence](http://w360confluence.mdc.cginet:8090/display/AR/Git+Contribution+Guide)***

### Pull Request

After you have completed your code task and pushed the code to your feature branch, a pull request should be made to the develop branch. This is to encourage peer to peer code review, allowing developers to learn amongst each other while keeping code style and standards consistent.
Reviewers are expected to take  1-2 business days to review and provide their feedback (special exceptions for very large commits). Reviewer are expected to merge the code back once approved. Authors / committers are responsible for re-basing if their pull request becomes out of date. Aforementioned, we will be adopting Gitflow model as part of our development process. However, instead of merging the code from feature branch to develop branch, we will be rebasing our feature code on top of develope branch. Please refer to the next section for more details.

***Note: for more details on how to create a pull request, please refer to [Confluence](http://w360confluence.mdc.cginet:8090/display/AR/Creating+a+Pull+Request)***

### Rebase

Git rebase is similar to a merge but it reapplies commits to the top of the branch. It is strongly recommended you rebase (and include a tag if a specific milestone needs to be recorded, i.e. production) instead of merging. Refer to the two diagrams below to see the difference between merging and rebasing.

<img src="http://w360confluence.mdc.cginet:8090/download/attachments/32014966/Screen%20Shot%202018-05-24%20at%2011.19.53%20AM.png" width="50%"><img src="http://w360confluence.mdc.cginet:8090/download/attachments/32014966/Screen%20Shot%202018-05-24%20at%2011.21.42%20AM.png" width="50%">

***Note: for more details, please refer to [Confluence](http://w360confluence.mdc.cginet:8090/display/AR/Git+Contribution+Guide)***

#### Rebasing in GitKraken

Please refer to [Confluence](http://w360confluence.mdc.cginet:8090/display/AR/GITKraken) page on how to rebase feature branch on top of develop branch using GitKraken. 
