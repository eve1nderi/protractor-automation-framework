import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  GlobalSearchCategory,
  GlobalSearchEntity,
  GlobalSearchGroupOptions,
  SideNavItem,
} from '@cgi/sentry-angular-components-lib';
import { Client } from '../../business-domain/client/shared/model/client';
import { ClientService } from '../../business-domain/client/shared/services/client.service';
import { ArrayList } from './../../shared/model/array-list';

/**
 * General navigation component.
 */
@Component({
  selector: 'cgi-app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.scss'],
})
export class AppShellComponent {
  /**
   * Side Nav Links:
   * For simplicity, an array of objects are created for navigation links instead of creating separate class and array of that typed class
   */
  links: SideNavItem[];

  json = [
    {
      icon: 'fa-home',
      text: 'Dashboard',
      link: 'home',
    },
    {
      icon: 'fa-users',
      text: 'Clients',
      link: 'clients',
    },
    {
      icon: 'fa-briefcase',
      text: 'Plans',
      link: 'plans',
    },
  ];

  /** globalSearchResult is used for giving search result to global search component */
  globalSearchResult: GlobalSearchGroupOptions[];
  /** globalSearchResultSize is used for giving size of globalSearchResult to global search component */
  globalSearchResultSize: number;
  /** Value for using Category or not */
  enableCategory = true;
  /** Value for setting up width of autocomplete */
  panelWidth = '20em';

  /** define boolean type for spinner */
  isLoading: boolean;
  /** saving SearchText Value for OnEnter */
  searchText: string;
  /** saving categoryInformation Value for OnEnter */
  categoryInformation: GlobalSearchCategory;
  /** saving isShowMore Value for OnEnter */
  isShowMore: boolean;
  /** saving entity Value for OnEnter */
  entity: GlobalSearchEntity[];

  /** Client List Data */
  clientListData: Client[];

  /** categories for drop down list for global search category selection */
  categories: GlobalSearchCategory[] = [{ name: 'Client', value: 'CLIENT' },
                                        { name: 'All', value: 'ALL' },
                                        { name: 'Plan', value: 'PLAN' },
                                      ];

  /**
   * Constructor to populate navigation items.
   */
  constructor(private route: ActivatedRoute,
              private router: Router,
              private clientService: ClientService) {

    this.links = this.fromJsonToSideNavItem(this.json);
  }

  private fromJsonToSideNavItem(json: any[]): SideNavItem[] {
    return json.map((item) => {
      let childSubsection = null;
      if (item.subsection && item.subsection.length > 0) {
        childSubsection = this.fromJsonToSideNavItem(item.subsection);
      }
      return new SideNavItem(item.text, item.link, item.icon, childSubsection);
    });
  }

  /**
   * Load the data for the autocomplete drop-down list of the global search component.
   * @param event event object
   */
  loadSearchResults(event: any) {
    if (
      event.globalSearch.get('searchString').value &&
      event.globalSearch.get('searchString').value.length > 0
    ) {
      this.isLoading = true;

      const filterData = event.globalSearch.get('searchString').value.toUpperCase();
      this.loadClientListData(filterData);

    } else {
      this.globalSearchResult = [];
    }
  }

  /**
   * When an option is selected from the autocomplete dropdown
   * @param event event object
   */
  onEnter(event: any) {
    this.searchText = event.searchText;
    this.categoryInformation = event.category;
    this.entity = event.entity;
    this.isShowMore = event.isShowMore;

    // search client by name case
    if (this.searchText != null &&
      this.categoryInformation.name === 'Clients') {
        // navigate to client list page
      const clientLastName = this.searchText;

      if (event.isShowMore) {
        // go to client list page
          this.router.navigate(['clients', { filterBy: clientLastName }], { relativeTo: this.route });
      } else if (event.entity != null) {
        // go to client detail page
        const sin = event.entity.code;
        const id = this.getClientCoidId(sin);
        if ( id != null && id .length > 0 ) {
          this.router.navigate(['client', id], { relativeTo: this.route });
        }
      }
    }

    // TODO ADD OTHER CASES...
  }

  /**
   * Function to reload client data based on search input filter.
   * @param filterData
   */
  loadClientListData(filterData: string) {
    const pageNumber = 0;
    const pageSize = 10;

    this.clientService.list(this.clientService.setQueryParams(filterData,
      pageNumber,
      pageSize,
      'clientName',
      'desc',
    )).subscribe((response) => {
      this.isLoading = false;
      this.convertSearchResponseData(response);
    });
  }

  /**
   * Analyse response from server, get data list and total number.
   * @param response response data from server
   */
  convertSearchResponseData( response: ArrayList<Client>) {
    if (response != null ) {
      this.clientListData = response.data;

      if (this.globalSearchResult == null || this.globalSearchResult.length < 1) {
        this.globalSearchResult = this.initGlobalSearchResult();
      }
      this.globalSearchResult[0].contents = this.convertToGlobalSearchEntity(response.data);
      this.globalSearchResultSize = response.data == null ? 0 : response.data.length;

      if ( response.metadata != null ) {
        this.globalSearchResult[0].totalRecords = response.metadata.totalRecords;
      }
    }
  }

  /**
   * Transfer server returned data to the GlobalSearchResult format
   * @param data: data returned from server
   */
  convertToGlobalSearchEntity(data: Client[]): GlobalSearchEntity[] {
    if (data) {
      return data.map((element) => {
        return new GlobalSearchEntity('SIN:' + element.sinOrBin, element.clientName);
      });
    }
    return [];
  }

  /**
   * Create a initial GlobalSearchResult object.
   */
  initGlobalSearchResult() {
    const searchResult: GlobalSearchGroupOptions[] = [];

    const globalSearchCategory: GlobalSearchCategory = new GlobalSearchCategory('Clients', 'CLIENTS');

    const globalSearchGroupOptions: GlobalSearchGroupOptions
        = new GlobalSearchGroupOptions([], 0, globalSearchCategory);

    searchResult.push(globalSearchGroupOptions);

    return searchResult;
  }

  /**
   * Return Client based on SIN value.
   * @param sin SIN value
   */
  getClientCoidId(sin: string) {

    const sinValue = sin.split(':')[1];
    const foundClient = this.clientListData.find((client) => {
      return (client.sinOrBin === sinValue);
    });

    return foundClient ? foundClient.clientCoidId : null;
  }
}
