import { Injectable } from '@angular/core';

/**
 * Example of a core service shared across products.
 */
@Injectable()
export class UserService {
  /**
   * Empty constructor.
   */
  constructor() {}
}
