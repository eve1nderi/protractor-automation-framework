import { CommonModule } from '@angular/common';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { CGIGlobalSearchModule, CGINavContainerModule, CGITopNavModule } from '@cgi/sentry-angular-components-lib';

import { AppShellComponent } from './app-shell/app-shell.component';
import { CoreRoutingModule } from './core-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserService } from './user.service';

/**
 * The core module
 *
 * This module is meant to bundle all cross-product components together.
 */

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    CGITopNavModule,
    CGIGlobalSearchModule,
    CGINavContainerModule,
    CoreRoutingModule,
  ],
  declarations: [AppShellComponent, PageNotFoundComponent],
  exports: [AppShellComponent],
  providers: [UserService],
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule,
  ) {
    // https://angular.io/guide/styleguide#prevent-re-import-of-the-core-module
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
