import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientCompositionComponent } from './../business-domain/client/client-composition/client-composition.component';
import { ClientListComponent } from './../business-domain/client/client-list/client-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: 'home',
    component: DashboardComponent,
  },
  {
    path: 'clients',
    component: ClientListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoreRoutingModule {}
