import { Component, OnInit } from '@angular/core';

/**
 * Component to present the page not found view to the user.
 */
@Component({
  selector: 'cgi-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss'],
})
export class PageNotFoundComponent implements OnInit {
  /**
   * Empty constructor.
   */
  constructor() {}

  /**
   * Empty ngOnInit.
   */
  ngOnInit() {}
}
