import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientCompositionComponent } from './client-composition/client-composition.component';
import { ClientListComponent } from './client-list/client-list.component';

const routes: Routes = [
  {
    path: 'clients',
    component: ClientListComponent,
  },
  {
    path: 'client/:id',
    component: ClientCompositionComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class ClientRoutingModule {}
