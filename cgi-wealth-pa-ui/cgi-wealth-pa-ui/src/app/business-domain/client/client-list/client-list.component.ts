import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ClientDataSource } from '../shared/services/client.datasource';
import { ClientService } from '../shared/services/client.service';
/**
 * Component for a client.
 */
@Component({
  selector: 'cgi-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss'],
})
export class ClientListComponent implements OnInit, AfterViewInit {
  /** Title property to present to the user. */
  title: string;
  /** Search box setup */
  clientTableSearchForm: FormGroup;
  /** Columns to display in table */
  displayedColumns = ['clientName', 'type', 'residency', 'sinBin'];
  /** Data source for table */
  dataSource: ClientDataSource;
  /** Reference to paginator directive */
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  /** Reference to sorting directive */
  @ViewChild(MatSort)
  sort: MatSort;
  /** Variable that stores the total records returned from the service */
  totalRecordCount: number;
  /** Boolean for if the table is loading */
  isLoading: boolean;
  /** Client Record Total Count */
  clientRecordCount: number;
  /** Last Name from Global Search Input */
  lastName: string;
  /** List Page Index for Initial Loading */
  pageNumber: number;
  /** List Page Size for Initial Loading */
  pageSize: number;
  /** List Page Size for Initial Loading */
  previousPageSize: number;
  /** Sorting direction for Initial Loading */
  sortOrder: string;
  /** Sorting Id  for Initial Loading */
  sortBy: string;
  /** Page Size Options */
  pageSizeOptions: any[];

  /**
   * Component Constructor
   * @param ClientService Service API to search clients
   */
  constructor(private clientService: ClientService,
              private route: ActivatedRoute,
              ) {}

  /**
   * Lifecycle hook for when the component is first initiated.
   */
  ngOnInit() {
    /**
     * Setup Initial Values for loading
     */
    this.title = 'Clients';
    this.lastName = '';
    this.pageNumber = 0;
    this.pageSize = 10;
    this.pageSizeOptions = [10, 20, 50];
    this.previousPageSize = 10;

    /**
     * Initial '/client/searchClients' API call
     * @param lastName
     * @param pageNumber
     * @param pageSize
     * @param sortBy
     * @param sortOrder
     */
    this.dataSource = new ClientDataSource(this.clientService);
    this.route.paramMap.subscribe((pMap) => {
      let filterByStr = this.route.snapshot.params['filterBy'];
      if ( filterByStr == null ) {
        filterByStr = '';
      }

      this.dataSource.searchClients(
          filterByStr.toUpperCase(),
          this.pageNumber,
          this.pageSize,
          'clientName',
          'desc',
        );
    });

    /**
     * Total Record Count
     */
    this.dataSource.totalRecordCount$.subscribe((data) => {
      this.totalRecordCount = data;
    });

    this.dataSource.loading$.subscribe((loading) => {
      this.isLoading = loading;
    });

    /**
     * Init Client Search
     */
    this.clientTableSearchForm = new FormGroup({
      tableSearch: new FormControl(),
    });
  }

  /**
   * Set the sort after the view init since this component will
   * be able to query its view for the initialized sort.
   */
  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(tap(() => this.loadClientsList()))
      .subscribe();
  }

  /** Load elements from the service based on supplied parameters */
  loadClientsList() {
    let sortOrder;
    let sortBy;
    let pageNumber = this.paginator.pageIndex;
    const pageSize = this.paginator.pageSize;

    if (this.previousPageSize !== pageSize) {
      /**
       * Items per Page
       * Move to the first page
       */
      pageNumber = 0;
      this.paginator.pageIndex = 0;
    }
    if (this.sort.direction) {
      sortOrder = this.sort.direction;
      sortBy = this.sort.active;
    } else {
      sortOrder = 'desc';
      sortBy = 'clientName';
    }

    /**
     * Set Current Page Size as Previous Page Size
     */
    this.previousPageSize = pageSize;

    /**
     * Sort or Pagination '/client/searchClients' API call
     * @param lastName
     * @param pageNumber
     * @param pageSize
     * @param sortBy
     * @param sortOrder
     */
    this.dataSource.searchClients(this.lastName, pageNumber, pageSize, sortBy, sortOrder);
  }

  /** Add new client button event */
  addNewClient() {}

}
