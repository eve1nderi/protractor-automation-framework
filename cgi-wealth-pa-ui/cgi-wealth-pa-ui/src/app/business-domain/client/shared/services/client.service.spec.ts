import { HttpClientModule } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';

import { ClientService } from './client.service';

describe('ClientService', () => {
  const IMPORTS = [HttpClientModule];
  const PROVIDERS = [ClientService];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: IMPORTS,
      providers: PROVIDERS,
    });
  });

  it('should be created', inject([ClientService], (service: ClientService) => {
    expect(service).toBeTruthy();
  }));
});
