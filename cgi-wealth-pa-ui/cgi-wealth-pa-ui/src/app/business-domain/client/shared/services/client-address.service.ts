import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { ArrayList } from '../../../../shared/model/array-list';
import { Metadata } from '../../../../shared/model/metadata';
import { BaseApiService } from '../../../../shared/services/base-api.service';
import { ClientAddress } from '../model/client-address';

/**
 * Client Address Service
 */
@Injectable()
export class ClientAddressService extends BaseApiService<ClientAddress> {
  /**
   * ClientAddressService constructor
   * @param httpClient HttpClient Dependency Injection
   */
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.apiUrl, 'address/getClientAddresses?clientCoidId=');
  }

  /**
   * Convert json data to specified object
   * @param json Json data to be converted
   */
  fromJsonToEntity(json: any): ClientAddress {
    return json.map((item) => ClientAddress.fromJSON(item));
  }

  /**
   * Convert json response to ArrayList object
   * @param metadata Metadata json object
   * @param data Actual json data requested
   */
  fromJsonToArrayList(metadata: any, data: any): ArrayList<ClientAddress> {
    return new ArrayList<ClientAddress>(
      Metadata.fromJSON(metadata),
      data.map((item) => ClientAddress.fromJSON(item)),
    );
  }

  /** Set Up Client Address Call Params */
  setQueryParams(clientCoidId) {
    return new HttpParams().set('clientCoidId', clientCoidId.toString());
  }
}
