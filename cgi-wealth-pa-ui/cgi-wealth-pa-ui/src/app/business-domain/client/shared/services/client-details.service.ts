import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { ArrayList } from '../../../../shared/model/array-list';
import { Metadata } from '../../../../shared/model/metadata';
import { BaseApiService } from '../../../../shared/services/base-api.service';
import { ClientDetail } from '../model/client-detail';

/**
 * Client Detail Service
 */
@Injectable()
export class ClientDetailService extends BaseApiService<ClientDetail> {
  /**
   * ClientDetailService constructor
   * @param httpClient HttpClient Dependency Injection
   */
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.apiUrl, 'client/getClientDetails?clientCoidId=');
  }
  /**
   * Convert json data to specified object
   * @param json Json data to be converted
   */
  fromJsonToEntity(json: any): ClientDetail {
    return ClientDetail.fromJSON(json);
  }

  /**
   * Convert json response to ArrayList object
   * @param metadata Metadata json object
   * @param data Actual json data requested
   */
  fromJsonToArrayList(metadata: any, data: any): ArrayList<ClientDetail> {
    return new ArrayList<ClientDetail>(
      Metadata.fromJSON(metadata),
      data.map((item) => ClientDetail.fromJSON(item)),
    );
  }

  /** Set Up Client Detail Call Params */
  setQueryParams(clientCoidId) {
    return new HttpParams().set('clientCoidId', clientCoidId.toString());
  }

  /**
   * Returns all Client classes
   */
  getClientClasses() {
    return ['Regular', 'Staff', 'Blank'];
  }

  /**
   * Returns all Client titles
   */
  getTitles() {
    return [
      'ABBE.',
      'ADJ.',
      'BRO.',
      'CADET.',
      'CANON.',
      'CAPT.',
      'CARD.',
      'CDMR.',
      'CHEF.',
      'CHIEF.',
      'CMDR.',
      'COL.',
      'CPL.',
      'DWO.',
      'DR.',
      'FR.',
      'GEN.',
      'HON.',
      'JUDGE.',
      'JUGE.',
      'LADY.',
      'LORD.',
      'LT.',
      'LTC.',
      'LORD.',
      'M.',
      'Marie.',
      'MAJ.',
      'Mayor MGR.',
      'MISS.',
      'MLLE.',
      'MME.',
      'MR.',
      'MRS.',
      'MS.',
      'MSGR.',
      'MSGT.',
      'MWO.',
      'PERE.',
      'PROF.',
      'PTE.',
      'RABBI.',
      'REF.',
      'REV.',
      'SEN.',
      'SGT.',
      'SIR.',
      'WO.s',
    ];
  }

  /**
   * Returns all Client Suffixes
   */
  getSuffixes() {
    return [
      '1ER',
      '1ST',
      'BA',
      'BBA',
      'BCOM',
      'BSC',
      'BSCN',
      'CA',
      'CGA',
      'DD',
      'DDS',
      'DMD',
      'ESQ',
      'FS',
      'II',
      'III',
      'IV',
      'JR',
      'LLB',
      'MA',
      'MBA',
      'MD',
      'MSC',
      'PENG',
      'PHD',
      'PR',
      'PRES',
      'QC',
      'RIA',
      'RN',
      'SR',
    ];
  }

  /**
   * Returns Genders
   */
  getGenders() {
    return ['Male', 'Female', 'Unknown'];
  }
  /**
   * Returns Countries
   */
  // tslint:disable-next-line
  getCountries() {
    return [
      {
        name: 'Canada',
      },
      {
        name: 'United States',
      },
      {
        name: 'United Kingdom',
      },
      {
        name: 'Italy',
      },
      {
        name: 'Australia',
      },
      {
        name: 'Austria',
      },
      {
        name: 'Bangladesh',
      },
      {
        name: 'Barbados',
      },
      {
        name: 'Belgium',
      },
      {
        name: 'Brazil',
      },
      {
        name: 'China',
      },
      {
        name: 'Cyprus ',
      },
      {
        name: 'Denmark ',
      },
      {
        name: 'Dominican Republic ',
      },
      {
        name: 'Egypt ',
      },
      {
        name: 'Finland ',
      },
      {
        name: 'France ',
      },
      {
        name: 'Germany ',
      },
      {
        name: 'India ',
      },
      {
        name: 'Indonesia ',
      },
      {
        name: 'Ireland ',
      },
      {
        name: 'Israel ',
      },
      {
        name: 'Ivory Coast ',
      },
      {
        name: 'Japan ',
      },
      {
        name: 'Republic of Korea ',
      },
      {
        name: 'Malaysia ',
      },
      {
        name: 'Morocco ',
      },
      {
        name: 'Netherlands ',
      },
      {
        name: 'New Zealand ',
      },
      {
        name: 'Norway ',
      },
      {
        name: 'Pakistan ',
      },
      {
        name: 'Philippines ',
      },
      {
        name: 'Romania ',
      },
      {
        name: 'Singapore ',
      },
      {
        name: 'Spain ',
      },
      {
        name: 'Sri Lanka ',
      },
      {
        name: 'Sweden ',
      },
      {
        name: 'Switzerland ',
      },
      {
        name: 'Thailand ',
      },
      {
        name: 'Tunisia ',
      },
      {
        name: 'Hong Kong ',
      },
      {
        name: 'Greece ',
      },
      {
        name: 'Kenya ',
      },
      {
        name: 'Malta ',
      },
      {
        name: 'Scotland ',
      },
      {
        name: 'Guyana ',
      },
      {
        name: 'Papua New Guinea',
      },
      {
        name: 'Trinidad Tobago ',
      },
      {
        name: 'Abu Dhabi ',
      },
      {
        name: 'Afghanistan ',
      },
      {
        name: 'Ajman ',
      },
      {
        name: 'Albania ',
      },
      {
        name: 'American Samoa ',
      },
      {
        name: 'Algeria ',
      },
      {
        name: 'Andorra ',
      },
      {
        name: 'Angola ',
      },
      {
        name: 'Anguilla ',
      },
      {
        name: 'Antarctica ',
      },
      {
        name: 'Antigua Barbuda ',
      },
      {
        name: 'Argentina  ',
      },
      {
        name: 'Aruba ',
      },
      {
        name: 'Bahamas ',
      },
      {
        name: 'Bahrain ',
      },
      {
        name: 'Benin ',
      },
      {
        name: 'Belize ',
      },
      {
        name: 'Bermuda ',
      },
      {
        name: 'Bhutan ',
      },
      {
        name: 'Bolivia ',
      },
      {
        name: 'Botswana ',
      },
      {
        name: 'Bouvet Island ',
      },
      {
        name: 'Brunei ',
      },
      {
        name: 'British Indian Ocean Territory ',
      },
      {
        name: 'British Virgin Islands ',
      },
      {
        name: 'Bulgaria ',
      },
      {
        name: 'Burkina Faso ',
      },
      {
        name: 'Burma ',
      },
      {
        name: 'Burundi ',
      },
      {
        name: 'Byelorussian Ssr ',
      },
      {
        name: 'Cameroon ',
      },
      {
        name: 'Cape Verde ',
      },
      {
        name: 'Cayman Islands ',
      },
      {
        name: 'Central African Republic ',
      },
      {
        name: 'Chad ',
      },
      {
        name: 'Chile ',
      },
      {
        name: 'Christmas Island ',
      },
      {
        name: 'Cocos Keeling ',
      },
      {
        name: 'Colombia ',
      },
      {
        name: 'Comoros ',
      },
      {
        name: 'Congo ',
      },
      {
        name: 'Cook Islands ',
      },
      {
        name: 'Costa Rica ',
      },
      {
        name: 'Cuba ',
      },
      {
        name: 'Djibouti ',
      },
      {
        name: 'Dominica ',
      },
      {
        name: 'Dubai ',
      },
      {
        name: 'Ecuador ',
      },
      {
        name: 'El Salvador ',
      },
      {
        name: 'Ethiopia ',
      },
      {
        name: 'Equator Guinea ',
      },
      {
        name: 'Falkland Island ',
      },
      {
        name: 'Faroe Island ',
      },
      {
        name: 'Fiji ',
      },
      {
        name: 'French Guyana ',
      },
      {
        name: 'French Polynesia ',
      },
      {
        name: 'French South Territory ',
      },
      {
        name: 'Gabon ',
      },
      {
        name: 'Gambia ',
      },
      {
        name: 'Ghana ',
      },
      {
        name: 'Gibraltar ',
      },
      {
        name: 'Greenland ',
      },
      {
        name: 'Granada ',
      },
      {
        name: 'Guadeloupe ',
      },
      {
        name: 'Guam ',
      },
      {
        name: 'Guatemala ',
      },
      {
        name: 'Guinea ',
      },
      {
        name: 'Guinea Bissau ',
      },
      {
        name: 'Haiti ',
      },
      {
        name: 'Hawaii ',
      },
      {
        name: 'Heard Island and McDonald Islands ',
      },
      {
        name: 'Honduras ',
      },
      {
        name: 'Hungary ',
      },
      {
        name: 'Iceland ',
      },
      {
        name: 'Iran ',
      },
      {
        name: 'Iraq ',
      },
      {
        name: 'Jordan ',
      },
      {
        name: 'Kiribati ',
      },
      {
        name: 'Democratic Republic of Korea ',
      },
      {
        name: 'Kuwait ',
      },
      {
        name: 'Laos ',
      },
      {
        name: 'Lebanon ',
      },
      {
        name: 'Lesotho ',
      },
      {
        name: 'Liberia ',
      },
      {
        name: 'Lib Jamahiriya ',
      },
      {
        name: 'Liechtenstein ',
      },
      {
        name: 'Luxembourg ',
      },
      {
        name: 'Macau ',
      },
      {
        name: 'Madagascar ',
      },
      {
        name: 'Malawi ',
      },
      {
        name: 'Maldives ',
      },
      {
        name: 'Mali ',
      },
      {
        name: 'Marshall Island ',
      },
      {
        name: 'Martinique ',
      },
      {
        name: 'Mauritania ',
      },
      {
        name: 'Mauritius ',
      },
      {
        name: 'Mexico ',
      },
      {
        name: 'Micronesia  ',
      },
      {
        name: 'Monaco ',
      },
      {
        name: 'Monilia ',
      },
      {
        name: 'Montserrat ',
      },
      {
        name: 'Mozambique ',
      },
      {
        name: 'Namibia ',
      },
      {
        name: 'Nauru ',
      },
      {
        name: 'Nepal ',
      },
      {
        name: 'Netherlands Antilles ',
      },
      {
        name: 'Neutral Zone ',
      },
      {
        name: 'New Caledonia ',
      },
      {
        name: 'Nicaragua ',
      },
      {
        name: 'Niger ',
      },
      {
        name: 'Nigeria ',
      },
      {
        name: 'Niu ',
      },
      {
        name: 'Norfolk Island ',
      },
      {
        name: 'North Mariana Island ',
      },
      {
        name: 'Oman ',
      },
      {
        name: 'Palau ',
      },
      {
        name: 'Panama ',
      },
      {
        name: 'Paraguay ',
      },
      {
        name: 'Peru ',
      },
      {
        name: 'Pitcairn ',
      },
      {
        name: 'Poland ',
      },
      {
        name: 'Prot Guinea ',
      },
      {
        name: 'Puerto Rico ',
      },
      {
        name: 'Qatar ',
      },
      {
        name: 'Zaire ',
      },
      {
        name: 'Reunion ',
      },
      {
        name: 'Rwanda ',
      },
      {
        name: 'St Martin FA ',
      },
      {
        name: 'St Helena ',
      },
      {
        name: 'San Marino ',
      },
      {
        name: 'Sao Tome Princ ',
      },
      {
        name: 'Saudi Arabia ',
      },
      {
        name: 'Senegal ',
      },
      {
        name: 'Seychelles ',
      },
      {
        name: 'Sharjah ',
      },
      {
        name: 'Sierra Leone ',
      },
      {
        name: 'Solomon Islands ',
      },
      {
        name: 'Somalia ',
      },
      {
        name: 'South Africa ',
      },
      {
        name: 'Spanish North Africa ',
      },
      {
        name: 'St Chris Nevis ',
      },
      {
        name: 'St Kitts Nevis ',
      },
      {
        name: 'St Lucia ',
      },
      {
        name: 'St Pierre Nevis ',
      },
      {
        name: 'St Vincent Grenada ',
      },
      {
        name: 'Sudan ',
      },
      {
        name: 'Suriname ',
      },
      {
        name: 'Sval Janma Island ',
      },
      {
        name: 'Swaziland ',
      },
      {
        name: 'Syrian Arab Republic ',
      },
      {
        name: 'Taiwan Province Of China ',
      },
      {
        name: 'Tanzania ',
      },
      {
        name: 'Togo ',
      },
      {
        name: 'Tokelau ',
      },
      {
        name: 'Tongo ',
      },
      {
        name: 'Turkey ',
      },
      {
        name: 'Turks Caicos Island ',
      },
      {
        name: 'Tuvalu ',
      },
      {
        name: 'Uganda ',
      },
      {
        name: 'Ukraine ',
      },
      {
        name: 'United Arab Emirate ',
      },
      {
        name: 'Us Min Out lying ',
      },
      {
        name: 'Uruguay ',
      },
      {
        name: 'Vanuatu ',
      },
      {
        name: 'Vatican City ',
      },
      {
        name: 'Venezuela ',
      },
      {
        name: 'Vietnam ',
      },
      {
        name: 'US Virgin Islands ',
      },
      {
        name: 'Wallis Futura ',
      },
      {
        name: 'Samoa ',
      },
      {
        name: 'West Sahara ',
      },
      {
        name: 'Yemen ',
      },
      {
        name: 'Yemen South ',
      },
      {
        name: 'Yugoslavia ',
      },
      {
        name: 'Zambia ',
      },
      {
        name: 'Zimbabwe ',
      },
      {
        name: 'Jamica ',
      },
      {
        name: 'Portugal ',
      },
      {
        name: 'Croatia ',
      },
      {
        name: 'Slovenia ',
      },
      {
        name: 'Russia ',
      },
      {
        name: 'Belarus ',
      },
      {
        name: 'Czech Republic ',
      },
      {
        name: 'Slovakia ',
      },
      {
        name: 'Latvia ',
      },
      {
        name: 'Estonia ',
      },
      {
        name: 'Armenia ',
      },
      {
        name: 'Azerbaijan ',
      },
      {
        name: 'Azores ',
      },
      {
        name: 'Cambodia ',
      },
      {
        name: 'Canary Islands ',
      },
      {
        name: 'Eritrea ',
      },
      {
        name: 'Georgia ',
      },
      {
        name: 'Guernsey ',
      },
      {
        name: 'Jersey ',
      },
      {
        name: 'Kazakhstan ',
      },
      {
        name: 'Kyrgyzstan ',
      },
      {
        name: 'Lithuania ',
      },
      {
        name: 'IsleOfMan ',
      },
      {
        name: 'Mayotte ',
      },
      {
        name: 'Moldova ',
      },
      {
        name: 'Bosnia ',
      },
      {
        name: 'Macedonia ',
      },
      {
        name: 'Myanmar ',
      },
      {
        name: 'Tajikistan ',
      },
      {
        name: 'South Georgia South San ',
      },
      {
        name: 'Turkmenistan ',
      },
      {
        name: 'Uzbekistan ',
      },
      {
        name: 'Palestinian Teritory ',
      },
      {
        name: 'Congo Democratic Republic ',
      },
      {
        name: ' Timor Leste   ',
      },
      {
        name: ' Serbia  ',
      },
      {
        name: ' Montenegro  ',
      },
      {
        name: ' Null  ',
      },
      {
        name: ' Saint Barthelemy  ',
      },
      {
        name: ' Bonaire St Eustatius Saba  ',
      },
      {
        name: ' Curacao  ',
      },
      {
        name: ' St Maarteen Dutch  ',
      },
      {
        name: ' Quebec  ',
      },
      {
        name: ' Canada Québec  ',
      },
      {
        name: ' Kampuchéa Rep',
      },
    ];
  }
}
