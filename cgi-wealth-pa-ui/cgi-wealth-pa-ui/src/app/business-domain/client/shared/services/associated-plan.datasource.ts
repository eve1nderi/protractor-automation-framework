import { BaseApiDataSource } from '../../../../shared/services/base-api.datasource';
import { AssociatedPlan } from '../model/associated-plan';
import { AssociatedPlanService } from './associated-plan.service';

/**
 * Custom implementation of Angular CDK DataSource for Data Table
 */
export class AssociatedPlanDataSource extends BaseApiDataSource<AssociatedPlan> {
  /**
   * Client Data Source Service Constructor
   * @param clientAddressService handles REST calls for searching clients
   */
  constructor(private assocPlansService: AssociatedPlanService) {
    super(assocPlansService);
  }

  /**
   * Responsible for handling query parameters, updating values for loading indicator and clients, and handle HTTP errors
   * @param clientCoidId
   */
  getAssociatedPlans(
    clientCoidId: string,
    sortBy: string,
    sortOrder: string) {
    this.loadData(this.assocPlansService.setQueryParams(clientCoidId, sortBy, sortOrder));
  }
}
