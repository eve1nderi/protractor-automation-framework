import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ClientAddress } from './../model/client-address';
import { ClientDetail } from './../model/client-detail';
import { ClientAddressService } from './client-address.service';
import { ClientDetailService } from './client-details.service';
import { ClientService } from './client.service';

/**
 * Client Sandbox Service and Data Store
 */
@Injectable()
export class ClientSandboxService {
  /**
   * Client Sandbox Service constructor
   * @param ClientDetailService for loading client Detail
   * @param ClientAddressService for loading client Detail
   */
  constructor(
    private clientDetailService: ClientDetailService,
    private clientAddressService: ClientAddressService,
  ) {}

  /**
   * Get Client Information by clientCoidId
   */
  getClientDetail(id): Observable<ClientDetail> {
    return this.clientDetailService.read(id).pipe(
      map((clientDetail) => {
        return clientDetail;
      }),
    );
  }

  /**
   * Get Client Address by clientCoidId
   */
  getClientAddress(id): Observable<ClientAddress> {
    return this.clientAddressService.read(id).pipe(
      map((clientAddress) => {
        return clientAddress;
      }),
    );
  }

}
