import { BaseApiDataSource } from '../../../../shared/services/base-api.datasource';
import { Client } from '../model/client';
import { ClientService } from './client.service';

/**
 * Custom implementation of Angular CDK DataSource for Data Table
 */
export class ClientDataSource extends BaseApiDataSource<Client> {
  /**
   * Client Data Source Service Constructor
   * @param clientService handles REST calls for searching clients
   */
  constructor(private clientService: ClientService) {
    super(clientService);
  }

  /**
   * Responsible for handling query parameters, updating values for loading indicator and clients, and handle HTTP errors
   * @param sortOrder
   * @param sortId
   * @param pageNumber
   * @param pageSize
   */
  searchClients(
    lastName: string,
    pageNumber: number,
    size: number,
    sortBy: string,
    sortOrder: string,
  ) {
    this.loadData(this.clientService.setQueryParams(lastName, pageNumber, size, sortBy, sortOrder));
  }
}
