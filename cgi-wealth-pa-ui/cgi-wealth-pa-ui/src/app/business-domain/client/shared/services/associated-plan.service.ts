import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { ArrayList } from '../../../../shared/model/array-list';
import { Metadata } from '../../../../shared/model/metadata';
import { BaseApiService } from '../../../../shared/services/base-api.service';
import { AssociatedPlan } from '../model/associated-plan';

/**
 * Associated Plan Service
 */
@Injectable()
export class AssociatedPlanService extends BaseApiService<AssociatedPlan> {
  /**
   * ClientAddressService constructor
   * @param httpClient HttpClient Dependency Injection
   */
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.apiUrl, '/client/getAssociatedPlans');
  }

  /**
   * Convert json data to specified object
   * @param json Json data to be converted
   */
  fromJsonToEntity(json: any): AssociatedPlan {
    return AssociatedPlan.fromJSON(json);
  }

  /**
   * Convert json response to ArrayList object
   * @param metadata Metadata json object
   * @param data Actual json data requested
   */
  fromJsonToArrayList(metadata: any, data: any): ArrayList<AssociatedPlan> {
    return new ArrayList<AssociatedPlan>(
      Metadata.fromJSON(metadata),
      data.map((item) => AssociatedPlan.fromJSON(item)),
    );
  }

  /** Set Up Associated plan Call Params */
  setQueryParams(clientCoidId, sortBy, sortOrder) {
    return new HttpParams().set('clientCoidId', clientCoidId.toString())
    .set('sortBy', sortBy.toString())
    .set('sortOrder', sortOrder.toString());
  }
}
