import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { ArrayList } from '../../../../shared/model/array-list';
import { Metadata } from '../../../../shared/model/metadata';
import { BaseApiService } from '../../../../shared/services/base-api.service';
import { Client } from '../model/client';

/**
 * Client Service
 */
@Injectable()
export class ClientService extends BaseApiService<Client> {
  /**
   * ClientService constructor
   * @param httpClient HttpClient Dependency Injection
   */
  constructor(httpClient: HttpClient) {
    super(httpClient, environment.apiUrl, 'client/searchClients');
  }

  /**
   * Convert json data to specified object
   * @param json Json data to be converted
   */
  fromJsonToEntity(json: any): Client {
    return Client.fromJSON(json);
  }

  /**
   * Convert json response to ArrayList object
   * @param metadata Metadata json object
   * @param data Actual json data requested
   */
  fromJsonToArrayList(metadata: any, data: any[]): ArrayList<Client> {
    return new ArrayList<Client>(
      Metadata.fromJSON(metadata),
      data.map((item) => Client.fromJSON(item)),
    );
  }

  /** Set Up Client List Call Params */
  setQueryParams(searchBy, pageNumber, pageSize, sortBy, sortOrder) {
    return new HttpParams()
      .set('searchBy', searchBy.toString())
      .set('pageNumber', pageNumber)
      .set('pageSize', pageSize)
      .set('sortBy', sortBy.toString())
      .set('sortOrder', sortOrder.toString());
  }
}
