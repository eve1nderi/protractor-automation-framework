/** Plan Entity */
export class AssociatedPlan {
  /** Plan Id */
  planId: string;
  /** Plan type */
  planType: string;
  /** Plan role */
  role: string;
  /** Plan status */
  status: string;
  /** Plan createdDate */
  creationDate: string;
  /** Plan adminUnit */
  adminUnitId: string;
  /** client id */
  accountCoidId: string;

  /**
   * Constructor for Plan Entity
   * @param Plan data for constructing the Plan
   */
  constructor(associatedPlan: any) {
    this.planId = associatedPlan.planId;
    this.planType = associatedPlan.planType;
    this.role = associatedPlan.role;
    this.status = associatedPlan.status;
    this.creationDate = associatedPlan.creationDate;
    this.adminUnitId = associatedPlan.adminUnitId;
    this.accountCoidId = associatedPlan.accountCoidId;
  }

  /** converting to Plan from JSON */
  static fromJSON(json: any): AssociatedPlan {
    return new AssociatedPlan(json);
  }
}
