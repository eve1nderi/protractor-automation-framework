import { Client } from './client';

/** Client Detail Entity */
export class ClientDetail extends Client {
  /** Client class */
  clientClass: string;
  /** Client XREF */
  clientXREF: string;
  /** Client Date of Birth */
  dateOfBirth: Date;
  /** Client Date of Death */
  dateOfDeath: Date;
  /** Client Gender */
  gender: string;
  /** Client Language */
  language: string;
  /** Client Organisation Name */
  organisationName: string;
  /** Client Suffix */
  suffix_: string;
  /** Client Title */
  title: string;

  /**
   * Constructor for Client Entity
   * @param client data for constructing the client detail
   */

  constructor(clientDetail: any) {
    super(clientDetail);
    this.clientClass = clientDetail.clientClass;
    this.clientXREF = clientDetail.clientXREF;
    this.dateOfBirth = clientDetail.dateOfBirth;
    this.dateOfDeath = clientDetail.dateOfDeath;
    this.firstName = clientDetail.firstName;
    this.gender = clientDetail.gender;
    this.language = clientDetail.language;
    this.lastName = clientDetail.lastName;
    this.organisationName = clientDetail.organisationName;
    this.residency = clientDetail.residency;
    this.secondName = clientDetail.secondName;
    this.sinOrBin = clientDetail.sinOrBin;
    this.suffix_ = clientDetail.suffix_;
    this.thirdName = clientDetail.thirdName;
    this.title = clientDetail.title;
    this.type = clientDetail.type;
  }

  /** converting to Client from JSON */
  static fromJSON(json: any): ClientDetail {
    return new ClientDetail(json);
  }
}
