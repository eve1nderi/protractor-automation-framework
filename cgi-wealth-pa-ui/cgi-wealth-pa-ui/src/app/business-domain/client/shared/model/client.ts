import { BaseEntity } from '../../../../shared/model/base-entity';

/** Client Entity */
export class Client extends BaseEntity {
  /** Client clientCoidId */
  clientCoidId: string;
  /** Client last name */
  lastName: string;
  /** Client first name */
  firstName: string;
  /** Client second name */
  secondName: string;
  /** Client third name */
  thirdName: string;
  /** Client type */
  type: string;
  /** Client residency */
  residency: string;
  /** Client sinOrBin */
  sinOrBin: string;
  /** Client clientName */
  clientName: string;

  /**
   * Constructor for Client Entity
   * @param client data for constructing the client
   */
  constructor(client: any) {
    super();
    this.clientCoidId = client.clientCoidId;
    this.lastName = client.lastName;
    this.firstName = client.firstName;
    this.sinOrBin = client.sinOrBin;
    this.type = client.type;
    this.residency = client.residency;
    this.sinOrBin = client.sinOrBin;
    this.clientName = client.clientName;
  }

  /** converting to Client from JSON */
  static fromJSON(json: any): Client {
    return new Client(json);
  }
}
