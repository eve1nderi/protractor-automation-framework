import { Client } from './client';

/** Client Address Entity */
export class ClientAddress extends Client {
  /** Address Line1 */
  addrLine1: string;
  /** Address Line2 */
  addrLine2: string;
  /** Apartment Number */
  apartmentNumber: string;
  /** City */
  city: string;
  /** Country */
  country: string;
  /** Effective End Date */
  effectiveEndDate: Date;
  /** Effective StartDate */
  effectiveStartDate: Date;
  /** Postal Code */
  postalCode: string;
  /** Purpose */
  purpose: string;
  /** Region */
  region: string;
  /** Seasonal Start Date */
  seasonalStartDate: string;
  /** Seasonal End Date */
  seasonalEndDate: string;

  /**
   * Constructor for Client Address Entity
   * @param client address data for constructing the client address
   */

  constructor(clientAddress: any) {
    super(clientAddress);
    this.addrLine1 = clientAddress.addrLine1;
    this.addrLine2 = clientAddress.addrLine2;
    this.apartmentNumber = clientAddress.apartmentNumber;
    this.city = clientAddress.city;
    this.country = clientAddress.country;
    this.effectiveEndDate = clientAddress.effectiveEndDate;
    this.effectiveStartDate = clientAddress.effectiveStartDate;
    this.postalCode = clientAddress.postalCode;
    this.purpose = clientAddress.purpose;
    this.region = clientAddress.region;
    this.seasonalStartDate = clientAddress.seasonalStartDate;
    this.seasonalEndDate = clientAddress.seasonalEndDate;
  }

  /** converting to Client from JSON */
  static fromJSON(json: any): ClientAddress {
    return new ClientAddress(json);
  }
}
