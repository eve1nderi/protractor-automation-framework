import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientCompositionComponent } from './client-composition.component';

describe('ClientCompositionComponent', () => {
  let component: ClientCompositionComponent;
  let fixture: ComponentFixture<ClientCompositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientCompositionComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientCompositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
