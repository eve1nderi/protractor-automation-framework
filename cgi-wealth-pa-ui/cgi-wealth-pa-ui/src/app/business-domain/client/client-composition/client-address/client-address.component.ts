import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ClientSandboxService } from '../../shared/services/client-sandbox.service';
import { ClientAddress } from './../../shared/model/client-address';

@Component({
  selector: 'cgi-client-address',
  templateUrl: './client-address.component.html',
  styleUrls: ['./client-address.component.scss'],
})
export class ClientAddressComponent implements OnInit, OnDestroy {
  /** Client detail Object */
  clientAddress: ClientAddress;
  /** Variable use to clean up subscriptions when component is destroyed */
  private ngUnsubscribe: Subject<void> = new Subject();
  constructor(private route: ActivatedRoute, private clientSandboxService: ClientSandboxService) {}
  /**
   * Lifecycle hook for when the component is first initiated.
   */
  ngOnInit() {
    /**
     * Initial 'client/getClientAddresses' API call
     * @param clientCoidId
     */
    const initialLoad$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.clientSandboxService.getClientAddress(params.get('id'))),
    );

    initialLoad$.subscribe((clientAddress) => {
      this.clientAddress = clientAddress;
    });
  }

  /** Component Destroy block */
  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  /**
   * Add Address for future uses
   */
  addAddress() {}
}
