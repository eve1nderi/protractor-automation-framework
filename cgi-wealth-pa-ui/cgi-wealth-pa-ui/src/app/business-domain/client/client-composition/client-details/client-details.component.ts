import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ClientDetail } from '../../shared/model/client-detail';
import { ClientDetailService } from '../../shared/services/client-details.service';
@Component({
  selector: 'cgi-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.scss'],
})
export class ClientDetailsComponent implements OnInit, OnChanges {
  /* variable for result */
  result: any;
  /** Reactive form object */
  reactiveForm: FormGroup;
  /** title object for dropdown list */
  titles = this.clientDetailService.getTitles();
  /** suffix object for dropdown list */
  suffixes = this.clientDetailService.getSuffixes();
  /** gender object for dropdown list */
  genders = this.clientDetailService.getGenders();
  /** country object for dropdown list */
  countries = this.clientDetailService.getCountries();
  /** clientClass object for dropdown list */
  clientClasses = this.clientDetailService.getClientClasses();
  /** Client detail Object */
  @Input()
  clientDetail: ClientDetail;
  /**
   * Component Constructor
   * @param fb form builder
   * @param clientDetailService
   */
  constructor(private fb: FormBuilder, private clientDetailService: ClientDetailService) {
    this.clientDetail = new ClientDetail({});
  }
  /**
   * Lifecycle hook for when the component is first initiated.
   */
  ngOnInit() {
    /** Initializing form controls */
    this.reactiveForm = this.fb.group({
      title: '',
      lastName: '',
      firstName: '',
      secondName: '',
      thirdName: '',
      suffix: '',
      sinOrBin: '',
      dateOfBirth: '',
      gender: '',
      language: '',
      residency: '',
      dateOfDeath: '',
      clientClass: '',
      clientXREF: '',
    });
  }
  /**
   * For on changes
   */
  ngOnChanges() {
    /* Assigning client detail data to each form control. */
    if (this.clientDetail) {
      this.reactiveForm.get('title').setValue(this.clientDetail.title);
      this.reactiveForm.get('lastName').setValue(this.clientDetail.lastName);
      this.reactiveForm.get('firstName').setValue(this.clientDetail.firstName);
      this.reactiveForm.get('secondName').setValue(this.clientDetail.secondName);
      this.reactiveForm.get('thirdName').setValue(this.clientDetail.thirdName);
      this.reactiveForm.get('suffix').setValue(this.clientDetail.suffix_);
      this.reactiveForm.get('sinOrBin').setValue(this.clientDetail.sinOrBin);
      this.reactiveForm.get('dateOfBirth').setValue(this.clientDetail.dateOfBirth);
      this.reactiveForm.get('gender').setValue(this.clientDetail.gender);
      this.reactiveForm.get('language').setValue(this.clientDetail.language);
      this.reactiveForm.get('residency').setValue(this.clientDetail.residency);
      this.reactiveForm.get('dateOfDeath').setValue(this.clientDetail.dateOfDeath);
      this.reactiveForm.get('clientClass').setValue(this.clientDetail.clientClass);
      this.reactiveForm.get('clientXREF').setValue(this.clientDetail.clientXREF);
    }
  }

  /**
   * For Save Button (Form Submit)
   */
  onSubmit() {
    this.result = this.reactiveForm.value;
  }

  /**
   * For Override Button
   */
  override() {}
}
