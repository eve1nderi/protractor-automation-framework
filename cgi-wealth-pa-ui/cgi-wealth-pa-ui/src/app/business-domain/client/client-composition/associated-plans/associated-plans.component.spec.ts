import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociatedPlansComponent } from './associated-plans.component';

describe('AssocPlansComponent', () => {
  let component: AssociatedPlansComponent;
  let fixture: ComponentFixture<AssociatedPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociatedPlansComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociatedPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
