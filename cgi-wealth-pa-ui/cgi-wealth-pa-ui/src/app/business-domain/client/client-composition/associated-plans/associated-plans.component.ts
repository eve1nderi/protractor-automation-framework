import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSort } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { AssociatedPlanDataSource } from '../../shared/services/associated-plan.datasource';
import { AssociatedPlanService } from '../../shared/services/associated-plan.service';

@Component({
  selector: 'cgi-associated-plans',
  templateUrl: './associated-plans.component.html',
  styleUrls: ['./associated-plans.component.scss'],
})
export class AssociatedPlansComponent implements OnInit, OnDestroy, AfterViewInit {
  /** Search box setup */
  clientTableSearchForm: FormGroup;
  /** client id for the component */
  clientCoidId: string;

  /** Columns to display in table */
  displayedColumns = ['planId', 'planType', 'role', 'status', 'creationDate', 'adminUnitId'];

  /** Data source for table */
  dataSource: AssociatedPlanDataSource;

  /** associated plan observable subject */
  private assocPlanDataSubject$: Subscription;

  @ViewChild(MatSort)
  sort: MatSort;

  /**
   * constructor service DI
   * @param assocPlanService
   * @param route
   */
  constructor(private assocPlansService: AssociatedPlanService, private route: ActivatedRoute) {}

  /**
   * Initial function to add plan data subscriber.
   */
  ngOnInit() {
    this.dataSource = new AssociatedPlanDataSource(this.assocPlansService);

    this.assocPlanDataSubject$ = this.route.paramMap.subscribe((data) => {
      this.clientCoidId = data.get('id');
      this.dataSource.getAssociatedPlans(this.clientCoidId, 'planId', 'desc');
    });

    this.clientTableSearchForm = new FormGroup({
      tableSearch: new FormControl(),
    });
  }

  /**
   * Life cycle function to un-subscriber.
   */
  ngOnDestroy() {
    this.assocPlanDataSubject$.unsubscribe();
  }

  /**
   * Set the sort after the view init since this component will
   * be able to query its view for the initialized sort.
   */
  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.loadClientsList());
  }

  /** Load elements from the service based on supplied parameters */
  loadClientsList() {
    let sortOrder;
    let sortBy;

    if (this.sort.direction) {
      sortOrder = this.sort.direction;
      sortBy = this.sort.active;
    } else {
      sortOrder = 'desc';
      sortBy = 'planId';
    }

    this.dataSource.getAssociatedPlans(this.clientCoidId, sortBy, sortOrder);
  }

  /**
   * Add new plan
   */
  addNewPlan() {}
}
