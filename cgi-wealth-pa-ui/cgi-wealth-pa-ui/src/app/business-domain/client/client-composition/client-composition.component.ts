import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ClientSandboxService } from '../shared/services/client-sandbox.service';
import { ClientDetail } from './../shared/model/client-detail';

@Component({
  selector: 'cgi-client-composition',
  templateUrl: './client-composition.component.html',
  styleUrls: ['./client-composition.component.scss'],
})
export class ClientCompositionComponent implements OnInit, OnDestroy {
  /** ClientName property to present to the user. */
  clientName = '';
  /** Client detail Object */
  clientDetail: ClientDetail;
  /** Variable use to clean up subscriptions when component is destroyed */
  private ngUnsubscribe: Subject<void> = new Subject();
  constructor(private route: ActivatedRoute, private clientSandboxService: ClientSandboxService) {}
  /**
   * Lifecycle hook for when the component is first initiated.
   */
  ngOnInit() {
    /**
     * Initial 'client/getClientDetails' API call
     * @param clientCoidId
     */
    const initialLoad$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.clientSandboxService.getClientDetail(params.get('id'))),
    );
    /**
     * Client Name to present to the user.
     */
    initialLoad$.subscribe((clientDetail) => {
      this.clientDetail = clientDetail;
      this.clientName = this.clientDetail.firstName + ', ' + this.clientDetail.lastName;
    });
  }

  bookmark() {}

  /** Component Destroy block */
  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
