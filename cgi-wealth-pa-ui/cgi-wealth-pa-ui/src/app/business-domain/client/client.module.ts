import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
} from '@angular/material';
import {
  CGIBreadcrumbsModule,
  CGIToasterComponent,
  CGIToasterModule,
} from '@cgi/sentry-angular-components-lib';

import { AssociatedPlansComponent } from './client-composition/associated-plans/associated-plans.component';
import { ClientAddressComponent } from './client-composition/client-address/client-address.component';
import { ClientCompositionComponent } from './client-composition/client-composition.component';
import { ClientDetailsComponent } from './client-composition/client-details/client-details.component';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientRoutingModule } from './client-routing.module';
import { AssociatedPlanService } from './shared/services/associated-plan.service';
import { ClientAddressService } from './shared/services/client-address.service';
import { ClientDetailService } from './shared/services/client-details.service';
import { ClientSandboxService } from './shared/services/client-sandbox.service';
import { ClientService } from './shared/services/client.service';

@NgModule({
  imports: [
    HttpClientModule,
    CGIBreadcrumbsModule,
    CGIToasterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    ClientRoutingModule,
  ],
  declarations: [
    ClientListComponent,
    ClientCompositionComponent,
    ClientDetailsComponent,
    ClientAddressComponent,
    AssociatedPlansComponent,
    AssociatedPlansComponent,
  ],
  providers: [
    AssociatedPlanService,
    HttpClientModule,
    ClientService,
    ClientDetailService,
    ClientAddressService,
    ClientSandboxService,
    MatDatepickerModule,
  ],
  entryComponents: [CGIToasterComponent],
})
export class ClientModule {}
