import { cloneDeep } from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';

/**
 * Base Store Service
 */
export class BaseStoreService<T> {
  private _state: BehaviorSubject<T>;

  /**
   * Default store constructor for setting initial state
   * @param initialState
   */
  constructor(initialState: T = null) {
    this._state = new BehaviorSubject(initialState);
  }

  /** public Observable for the store */
  get state$(): Observable<T> {
    return this._state.asObservable();
  }

  /**
   * Get current store value
   */
  getState(): T {
    return this._state.getValue();
  }

  /**
   * Set data in the store
   * @param nextState set value for the data in the store
   */
  setState(nextState: T): void {
    this._state.next(cloneDeep(nextState));
  }
}
