import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ArrayList } from './../model/array-list';
import { BaseEntity } from './../model/base-entity';

/** Generic entity service for handling basic CRUD */
export abstract class BaseApiService<T extends BaseEntity> {
  /**
   * Constructor for the generic entity service
   * @param httpClient for http calls
   * @param url API url
   * @param endpoint API endpoint
   */
  constructor(protected httpClient: HttpClient, protected url: string, public endpoint: string) {}

  /** Creating a entity */
  create(item: T): Observable<T> {
    return this.httpClient
      .post<T>(`${this.url}/${this.endpoint}`, item)
      .pipe(map((json: any) => this.fromJsonToEntity(json as T)));
  }

  /** Updating a entity */
  update(item: T): any {
    return this.httpClient.put<T>(`${this.url}/${this.endpoint}/${item}`, item);
  }

  /** Getting a entity */
  read(id: number): Observable<T> {
    return this.httpClient
      .get(`${this.url}/${this.endpoint}${id}`)
      .pipe(map((json: any) => this.fromJsonToEntity(json.resultData)));
  }

  /** Getting a list of entities */
  list(queryParams): Observable<ArrayList<T>> {
    return this.httpClient
      .get(`${this.url}/${this.endpoint}`, { params: queryParams })
      .pipe(
        map((json: any) => this.fromJsonToArrayList(json.metadata || {}, json.resultData || [])),
      );
  }

  /** abstract method for converted json to entity */
  abstract fromJsonToEntity(json: any): T;

  /** abstract method for converting metadata and data from the json response to an array list */
  abstract fromJsonToArrayList(metadata: any, data: any[]): ArrayList<T>;
}
