/* tslint:disable no-identical-functions */
import { Injectable, ViewContainerRef } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { CGIToasterComponent } from '@cgi/sentry-angular-components-lib';

/** Shared service in charge of handling application-wide notifications */

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  /**
   * generic constructor including snackbar for notifications
   * @param snackBar
   */
  constructor(private snackBar: MatSnackBar) {}

  /**
   * Show toaster message for errors
   * @param messageString message to be displayed
   * @param viewContainerRef anchor element that specifies containing view
   * ie. cgi-toaster-warning, cgi-toaster-success, cgi-toaster-error
   */
  error(messageString: string, viewContainerRef: ViewContainerRef) {
    const config = new MatSnackBarConfig();
    config.panelClass = 'cgi-toaster-error';
    config.viewContainerRef = viewContainerRef;
    config.data = { message: messageString };
    this.openToaster(config);
  }

  /**
   * Show toaster message when successful
   * @param messageString message to be displayed
   * @param viewContainerRef anchor element that specifies containing view
   * ie. cgi-toaster-warning, cgi-toaster-success, cgi-toaster-error
   */
  success(messageString: string, viewContainerRef: ViewContainerRef) {
    const config = new MatSnackBarConfig();
    config.panelClass = 'cgi-toaster-success';
    config.viewContainerRef = viewContainerRef;
    config.data = { message: messageString };
    this.openToaster(config);
  }

  /**
   * Open toaster based on config
   * @param config snack bar config
   */
  private openToaster(config: MatSnackBarConfig) {
    config.verticalPosition = 'top';
    config.horizontalPosition = 'center';
    config.duration = 0;
    this.snackBar.openFromComponent(CGIToasterComponent, config);
  }
}
