import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';
import { BaseApiService } from '../services/base-api.service';
import { BaseEntity } from './../model/base-entity';

/**
 * Class to extend when creating a datasource for use with a MatTable.
 * Type supplied is the model representing the data to be displayed in the table.
 */
export abstract class BaseApiDataSource<T extends BaseEntity> implements DataSource<T> {
  /** Data that will be displayed in the table */
  private _dataSubject = new BehaviorSubject<T[]>([]);
  /** Subject indicating whether the process has finished loading */
  private _loadingSubject = new BehaviorSubject<boolean>(false);
  /** Subject to indicate how many record were retrieved from the query to the service */
  private _totalRecordCountSubject = new BehaviorSubject<number>(0);

  /** Observable Data that will be displayed in the table */
  public data$ = this._dataSubject.asObservable();
  /** Observable indicating whether the process has finished loading */
  public loading$ = this._loadingSubject.asObservable();
  /** Observable to indicate how many record were retrieved from the query to the service */
  public totalRecordCount$ = this._totalRecordCountSubject.asObservable();
  /**
   * Component Constructor
   * @param baseService Base Service API to handle CRUD actions
   */
  constructor(private baseService: BaseApiService<T>) {}

  /** For connecting to the matTable */
  connect(): Observable<T[]> {
    return this._dataSubject.asObservable();
  }

  /**
   * unsubscribe from the subjects to prevent memory leaks
   */
  disconnect() {
    this._loadingSubject.complete();
    this._dataSubject.complete();
    this._totalRecordCountSubject.complete();
  }

  /**
   * Calls the service method dataSourceList to load the data. Updates loading$ and assigns new data to $data.
   * @param queryParams Object of the parameters that are to be passed to the service. Passed to the method dataSourceList
   *               that is implemented in a DataSourceService.
   */
  loadData(queryParams) {
    this._loadingSubject.next(true);

    this.baseService
      .list(queryParams)
      .pipe(
        map((res) => {
          if (res.metadata.pageNumber === 0) {
            this._totalRecordCountSubject.next(res.metadata.totalRecords);
          }
          return res.data;
        }),
        catchError(() => {
          this._totalRecordCountSubject.next(0);
          return of([]);
        }),
        finalize(() => this._loadingSubject.next(false)),
      )
      .subscribe((data) => {
        this._dataSubject.next(data || []);
      });
  }

  /** Array of the values queried */
  getDataList(): T[] {
    return this._dataSubject.getValue();
  }
}
