/**
 * Generic class for list metadata.
 */
export class Metadata {
  /**
   * Constructor for Metadata
   * @param pageNumber page number
   * @param pageSize size of record per page
   * @param totalPages total number of pages
   * @param totalRecords total number of records
   */
  constructor(
    public pageNumber: number = 0,
    public pageSize: number = 0,
    public totalPages: number = 0,
    public totalRecords: number = 0,
  ) {
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.totalPages = totalPages;
    this.totalRecords = totalRecords;
  }

  /**
   * Convert Json to object static method
   * @param json
   */
  static fromJSON(json: any): Metadata {
    return new Metadata(
      json.pageNumber,
      json.pageSize,
      json.totalPages,
      json.totalRecords,
    );
  }
}
