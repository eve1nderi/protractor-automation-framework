import { Metadata } from './metadata';

/**
 * Generic class for list responses.
 * T is your list model
 */
export class ArrayList<T> {
  /**
   * Constructor for ArrayList type of data
   * @param metadata Metadata containing about paging information
   * @param data payload
   */
  constructor(public metadata: Metadata, public data: T[]) {
    this.metadata = metadata,
    this.data = data;
  }
}
