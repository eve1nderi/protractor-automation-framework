import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [CommonModule],
  declarations: [],
  exports: [CommonModule], // Export all symbols that other feature modules need to use
})
export class SharedModule {}
