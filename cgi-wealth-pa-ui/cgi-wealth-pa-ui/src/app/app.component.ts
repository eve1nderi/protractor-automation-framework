import { Component } from '@angular/core';

/**
 * Root component for all CGI applications.
 */
@Component({
  selector: 'cgi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {}
