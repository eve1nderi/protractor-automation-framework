import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientModule } from './business-domain/client/client.module';
import { CoreModule } from './core/core.module';
import { DashboardComponent } from './core/dashboard/dashboard.component';

/**
 * The bootstrapper module
 */
@NgModule({
  declarations: [AppComponent, DashboardComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    ClientModule,
    AppRoutingModule, // Eagerly load feature module before AppRoutingModule if feature module also has its own routing module
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
