// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html
const path = require('path');
const base = path.resolve(__dirname, '..');
const currentDate = new Date();

const reportDir = `${base}/reports/unit/${currentDate.getDate()}-${currentDate.getMonth() +
  1}-${currentDate.getFullYear()}T${currentDate.getHours()}-${currentDate.getMinutes()}-${currentDate.getSeconds()}`;
const chromeDir = `${base}/chrome-data`;

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-htmlfile-reporter'),
      require('karma-junit-reporter'),
      require('@angular-devkit/build-angular/plugins/karma'),
    ],
    client: {
      clearContext: false, // leave Jasmine Spec Runner output visible in browser
    },
    reporters: ['progress', 'junit', 'kjhtml', 'html'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    // switch to 'Chrome' to run with GUI
    browsers: ['ChromeNoSandbox'],
    customLaunchers: {
      ChromeNoSandbox: {
        base: 'Chrome',
        flags: [
          '--no-sandbox', // required to run without privileges in docker
          `--user-data-dir=${chromeDir}`,
          '--disable-gpu',
          '--disable-web-security',
        ],
      },
      ChromeNoSandboxHeadless: {
        base: 'ChromeHeadless',
        flags: [
          '--no-sandbox', // required to run without privileges in docker
          `--user-data-dir=${chromeDir}`,
          '--disable-gpu',
          '--disable-web-security',
        ],
      },
    },
    singleRun: false,
    files: [
      // Custom theming in tests not yet supported
      // https://github.com/angular/material2/issues/4056
      {
        pattern: '../node_modules/@angular/material/prebuilt-themes/indigo-pink.css',
        included: true,
        watched: true,
      },
    ],
    junitReporter: {
      outputDir: reportDir,
      outputFile: 'karma-tests.xml',
      useBrowserName: false,
    },
    htmlReporter: {
      outputFile: `${reportDir}/units.html`,

      // Optional
      pageTitle: 'Unit Tests',
      subPageTitle: 'Plan360',
      groupSuites: true,
      useCompactStyle: true,
      useLegacyStyle: false,
    },
  });
};
