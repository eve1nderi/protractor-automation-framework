// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const mkdirp = require('mkdirp');
const path = require('path');
const fs = require('fs');

const base = path.resolve(__dirname, '..');
const currentDate = new Date();

const reportDir = `${base}/reports/e2e/${currentDate.getDate()}-${currentDate.getMonth() +
  1}-${currentDate.getFullYear()}T${currentDate.getHours()}-${currentDate.getMinutes()}-${currentDate.getSeconds()}`;
const screenshots = `${reportDir}/screenshots`;
const htmlReport = `${reportDir}/report.html`;
const jsonReport = `${reportDir}/report.json`;
const testsDir = `${__dirname}/tests`;
const chromeDir = `${base}/chrome-data`;

exports.config = {
  allScriptsTimeout: 20000,
  getPageTimeout: 20000,
  specs: [`${testsDir}/**/*.e2e-spec.ts`],
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: [
        '--no-sandbox',
        '--disable-gpu',
        `--user-data-dir=${chromeDir}`,
        '--log-level=2',
        '--disable-web-security',
       // '--headless',
      ],
    },
  },
  directConnect: true,
  // baseUrl: 'http://localhost:4200',
  baseUrl: 'https://angular.io',

  // Specs here are the cucumber feature files
  specs: [`${testsDir}/features/**/*.feature`],
  SELENUIM_PROMISE_MANAGER: false,
  // Use a custom framework adapter and set its relative path
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  // cucumber command line options
  cucumberOpts: {
    // require step definition files before executing features
    require: [`${testsDir}/util/**/*.ts`, `${testsDir}/steps/**/*steps.ts`],
    // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    tags: [],
    // <boolean> fail if there are any undefined or pending steps
    strict: true,
    // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
    format: ['json:' + jsonReport],
    // <boolean> invoke formatters without executing steps
    dryRun: false,
    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    compiler: [],
  },

  // Enable TypeScript for the tests
  onPrepare() {
    if (!fs.existsSync(reportDir)) {
      mkdirp.sync(reportDir);
    }

    browser.driver
      .manage()
      .window()
      .maximize();
    require('ts-node').register({
      project: `${__dirname}/tsconfig.e2e.json`,
    });
  },
  onCleanUp() {
    var CucumberHtmlReport = require('cucumber-html-reporter');

    var options = {
      theme: 'bootstrap',
      jsonFile: jsonReport,
      output: htmlReport,
      screenshotsDirectory: screenshots,
      storeScreenshots: false,
      reportSuiteAsScenarios: true,
    };

    CucumberHtmlReport.generate(options);
  },
};
