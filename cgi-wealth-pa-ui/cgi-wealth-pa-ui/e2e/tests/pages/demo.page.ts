import { browser, by, element, until } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  enterSearchInput(text: string) {
    return element(by.css('input[aria-label="search"]')).sendKeys(text);
  }

  getSearchResultItems() {
    const condition = until.elementsLocated(by.css('.search-results .search-result-item'));

    return browser.wait(condition, 5000);
  }
}
