import { setDefaultTimeout, setWorldConstructor } from 'cucumber';
import { browser } from 'protractor';
/**
 * Custom world implementation.
 * When you want to use world in your steps or hooks utilize "function() {}" syntax instead of "() => {}".
 * Arrow functions do not have "this" context.
 */
export class CustomWorld {
  attach;
  parameters;

  constructor({ attach, parameters }) {
    this.attach = attach;
    this.parameters = parameters;
  }
  /**
   * Takes a screenshot and attaches it to the current step folder.
   */
  async takeScreenshot() {
    await this.attach(await browser.takeScreenshot(), 'image/png');
  }
}
setWorldConstructor(CustomWorld);
setDefaultTimeout(20000);
