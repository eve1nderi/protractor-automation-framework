import { expect } from 'chai';
import { Before, Given, Then, When } from 'cucumber';
import { browser } from 'protractor';
import { AppPage } from '../pages/demo.page';

let app;
const world = this;

Before(() => {
  app = new AppPage();
});

Given('I am on the angular.io site', () => app.navigateTo());

When('I type {string} into the search input field', async function(text: string) {
  await app.enterSearchInput(text);
  await this.takeScreenshot();
});

Then('I should see some results in the search overlay', () =>
  app.getSearchResultItems().then((elems) => expect(elems.length).to.be.greaterThan(0)),
);
