/**
* A Jenkinsfile is used to declare the build and deployment steps for a project.
* There are several parameters to be configured:
* 1. SNAPSHOT_VERSION - versioning of project, for nexus history
* 2. PROJECT_NAME - used for deployment and naming of artifacts
*
* @author eric.wong
*/

SNAPSHOT_VERSION = "1.1.0-SNAPSHOT"
PROJECT_NAME = "cgi-wealth-pa-ui"
NEXUS_URL = "http://w360tm1.mdc.cginet:8081/repository/pa360-snapshots/"
DEPLOY_SERVER = "pa101dev1.mdc.cginet"
DEPLOY_PATH = "/usr/tomcat/default/apache-tomcat-8.5.4/webapps"

node('panode') { // determines which VM/Container to run the build steps
  sh 'npm --version'
  sh 'ng -v'
  // try { // Enable this try and catch block if you want to send out email and slack notification
    stage('Checkout branch') { // checkout code
        checkout scm;
    }
    stage('Install') { // run any pre-build installation, dependency setup
        sh 'npm install'
    }
    stage('Test') { // execute testing strategy for Angular projects
        try {
            sh 'npm run docs:coverage'
            sh 'ng lint'
            //sh 'ng test --code-coverage --watch=false'
        } finally {
            //junit 'reports/*.xml' // will provide reporting to Jenkins UI to display to the user
        }
    }
    stage('Run Sonar Analysis') { // has dependency on the test stage to complete to provide coverage reporting
        dir("$WORKSPACE") {
            sh 'echo  >> sonar-project.properties'
            sh "echo 'sonar.branch=${env.BRANCH_NAME}' >> sonar-project.properties" // to uniquely identify the sonar project report
            sh 'npm i sonar-scanner --save-dev' // library used to run the scan
            withSonarQubeEnv('SonarQubeServer') { // will provide reporting to Jenkins UI to display to the user
                sh 'npm run sonar' // requires `sonar` script to be available in package.json`
            }
        }
    }
	  stage("Quality Gate") {
        sleep 20 // wait for sonarqube to return sonar test status back to jenkins.
        def QUALITY_GATE = waitForQualityGate()
        if (QUALITY_GATE.status != 'OK') {
            error "Pipeline aborted due to quality gate failure: ${QUALITY_GATE.status}"

            // Uncomment this section if you want to enable email and slack notification.
            // Please contact DevOps on how to set up email and slack integration before uncommenting the lines
            /* if (UPDATE_JIRA_AUTOMATED=='true'){
              jiraIssueSelector(issueSelector: [$class: 'DefaultIssueSelector'])
              // Jenkins URL to the job. Replace this url with the your corresponding Jenkins job for your project.
              sh 'curl -s "http://jenkins.w360ksstg.cginet/job/Wealth360-Digital-Technology/job/cgi-amazo-boilerplate/${BUILD_NUMBER}/api/xml?wrapper=changes&xpath=//changeSet//comment" > scm_output'

              getJiraTicket() //from shared lib

              emailext mimeType: 'text/html', subject: "BUILD ${JIRA} FAILED Amazo Boilerplate", body: "<p>Jira tickets:${JIRA}</p> <p>Build ID:${env.BUILD_TAG}</p> <p>Check build at: ${env.BUILD_URL}</p>", to: "$EMAIL_RECIPIENTS"

              // Slack channel to publish the notification to. Replace this url with the desired slack channel URL.
              slackSend baseUrl: 'https://cgi-global-wealth.slack.com/services/hooks/jenkins-ci/', channel: 'digital-technology', color: 'danger', message: "Build ${env.BUILD_TAG} FAILED for Jira tickets:${JIRA} at(<${env.BUILD_URL}|Jenkins Link>)", tokenCredentialId: 'slack-integration-digitaltech'
            } else {
              emailext mimeType: 'text/html', subject: "BUILD ${env.BUILD_TAG} FAILED Amazo Boilerplate", body: "<p>Check build at: ${env.BUILD_URL}</p>", to: "$EMAIL_RECIPIENTS"

              // Slack channel to publish the notification to. Replace this url with the desired slack channel URL.
              slackSend baseUrl: 'https://cgi-global-wealth.slack.com/services/hooks/jenkins-ci/', channel: 'digital-technology', color: 'danger', message: "Build ${env.BUILD_TAG} FAILED for Jira tickets:${JIRA} at(<${env.BUILD_URL}|Jenkins Link>)", tokenCredentialId: 'slack-integration-digitaltech'
            } */
        } else {
            echo 'Proceed with Build'
        }
	  }
    /** This section will require customized configuration based on build and deployment needs */
    if (env.BRANCH_NAME == 'master') { // conditional build and deploy based on branch
        stage('Build') {
            sh 'npm run docs:build'
            sh "ng build --configuration=production --base-href=/${PROJECT_NAME}/stable/ --deploy-url=/${PROJECT_NAME}/stable/"
        }
        stage('Tar Files') { // package files for deployment purposes
            dir("$WORKSPACE/dist") {
                sh "tar -cvf ${PROJECT_NAME}.tar * "
            }
            dir("$WORKSPACE/documentation") {
                sh "tar -cvf ${PROJECT_NAME}-documentation.tar * "
            }
        }
        stage('Deploy to Nexus') {
            dir("$WORKSPACE") {
                sh "mvn deploy:deploy-file -DgroupId=com.cgi.wealth.pa -DartifactId=${PROJECT_NAME} -Dversion=${SNAPSHOT_VERSION} -DgeneratePom=true -Dpackaging=tar -DrepositoryId=nexus -Durl=${NEXUS_URL} -Dfile=dist/${PROJECT_NAME}.tar -q"
                sh "mvn deploy:deploy-file -DgroupId=com.cgi.wealth.pa -DartifactId=${PROJECT_NAME}-documentation -Dversion=${SNAPSHOT_VERSION} -DgeneratePom=true -Dpackaging=tar -DrepositoryId=nexus -Durl=${NEXUS_URL} -Dfile=documentation/${PROJECT_NAME}-documentation.tar -q"
            }
        }
        stage ('Deploy to environment') {
            node("PA101DEV1") { // specify environment name as declared in Jenkins environment (check with DEVOPS)
                // retrieve metadata for JIRA as well as for retrieving the latest deployed artifacts
                sh "curl -s '${NEXUS_URL}/com/cgi/wealth/pa/${PROJECT_NAME}/${SNAPSHOT_VERSION}/maven-metadata.xml' >test"
                sh 'grep "<value>.*</value>" test | uniq | sed -e "s/[</value>]//g" | tr -d " \t\n\r\f" > output'
                def OUTPUT = readFile 'output'

                sh "curl -s '${NEXUS_URL}/com/cgi/wealth/pa/${PROJECT_NAME}-documentation/${SNAPSHOT_VERSION}/maven-metadata.xml' >test1"
                sh 'grep "<value>.*</value>" test1 | uniq | sed -e "s/[</value>]//g" | tr -d " \t\n\r\f" > output1'
                def OUTPUT1 = readFile 'output1'

                // must execute shell script in batch to ensure context is kept when sudo-ing to another user
                sh """ sudo pamgr <<EOF
                rm -rf ${DEPLOY_PATH}/${PROJECT_NAME}/stable/*
                mkdir ${DEPLOY_PATH}/${PROJECT_NAME}/stable/documentation
                wget --progress=bar:force ${NEXUS_URL}/com/cgi/wealth/pa/${PROJECT_NAME}/${SNAPSHOT_VERSION}/${PROJECT_NAME}-${OUTPUT}.tar -P ${DEPLOY_PATH}/${PROJECT_NAME}/stable/

                wget --progress=bar:force ${NEXUS_URL}/com/cgi/wealth/pa/${PROJECT_NAME}-documentation/${SNAPSHOT_VERSION}/${PROJECT_NAME}-documentation-${OUTPUT1}.tar -P ${DEPLOY_PATH}/${PROJECT_NAME}/stable/documentation/

                tar xf ${DEPLOY_PATH}/${PROJECT_NAME}/stable/${PROJECT_NAME}*.tar -C ${DEPLOY_PATH}/${PROJECT_NAME}/stable/
                tar xf ${DEPLOY_PATH}/${PROJECT_NAME}/stable/documentation/${PROJECT_NAME}-documentation*.tar -C ${DEPLOY_PATH}/${PROJECT_NAME}/stable/documentation
EOF
                """
            }
        }
    }
    if (env.BRANCH_NAME == 'develop') {
        stage('Build') {
            sh 'npm run docs:build'
            sh "ng build --configuration=production --base-href=/${PROJECT_NAME}/latest/ --deploy-url=/${PROJECT_NAME}/latest/"
        }
        stage('Tar Files') {
            dir("$WORKSPACE/dist") {
                sh "tar -cvf ${PROJECT_NAME}.tar * "
            }
            dir("$WORKSPACE/documentation") {
                sh "tar -cvf ${PROJECT_NAME}-documentation.tar * "
            }
        }
        stage ('Deploy to environment') {
            dir("$WORKSPACE") {
                sh "scp dist/${PROJECT_NAME}*.tar pamgr@${DEPLOY_SERVER}:${DEPLOY_PATH}/${PROJECT_NAME}/latest/"
                sh "scp documentation/${PROJECT_NAME}*.tar pamgr@${DEPLOY_SERVER}:${DEPLOY_PATH}/${PROJECT_NAME}/latest/"
            }
            node("PA101DEV1") {
                sh """ sudo pamgr <<EOF
                rm -rf ${DEPLOY_PATH}/${PROJECT_NAME}/latest/*.js
                rm -rf ${DEPLOY_PATH}/${PROJECT_NAME}/latest/3rdpartylicenses.txt
                rm -rf ${DEPLOY_PATH}/${PROJECT_NAME}/latest/favicon.ico
                rm -rf ${DEPLOY_PATH}/${PROJECT_NAME}/latest/index.html
                rm -rf ${DEPLOY_PATH}/${PROJECT_NAME}/latest/*.css
                tar xf ${DEPLOY_PATH}/${PROJECT_NAME}/latest/${PROJECT_NAME}.tar -C ${DEPLOY_PATH}/${PROJECT_NAME}/latest/
                mkdir -p ${DEPLOY_PATH}/${PROJECT_NAME}/latest/documentation
                tar xf ${DEPLOY_PATH}/${PROJECT_NAME}/latest/${PROJECT_NAME}-documentation.tar -C ${DEPLOY_PATH}/${PROJECT_NAME}/latest/documentation
EOF
                """
            }
        }
    }
  /* } catch (Exception e) { // Enable this try and catch block if you want to send out email and slack notification
    currentBuild.result = "FAILED"
    if (UPDATE_JIRA_AUTOMATED=='true'){
      jiraIssueSelector(issueSelector: [$class: 'DefaultIssueSelector'])
      // Jenkins URL to the job. Replace this url with the your corresponding Jenkins job for your project.
      sh 'curl -s "http://jenkins.w360ksstg.cginet/job/Wealth360-Digital-Technology/job/cgi-amazo-boilerplate/${BUILD_NUMBER}/api/xml?wrapper=changes&xpath=//changeSet//comment" > scm_output'

      getJiraTicket() //from shared lib

      emailext mimeType: 'text/html', subject: "BUILD ${JIRA} FAILED Amazo Boilerplate", body: "<p>Jira tickets:${JIRA}</p> <p>Build ID:${env.BUILD_TAG}</p> <p>Check build at: ${env.BUILD_URL}</p>", to: "$EMAIL_RECIPIENTS"

      // Slack channel to publish the notification to. Replace this url with the desired slack channel URL.
      slackSend baseUrl: 'https://cgi-global-wealth.slack.com/services/hooks/jenkins-ci/', channel: 'digital-technology', color: 'danger', message: "Build ${env.BUILD_TAG} FAILED for Jira tickets:${JIRA} at(<${env.BUILD_URL}|Jenkins Link>)", tokenCredentialId: 'slack-integration-digitaltech'
    } else {
      emailext mimeType: 'text/html', subject: "BUILD ${env.BUILD_TAG} FAILED Amazo Boilerplate", body: "<p>Check build at: ${env.BUILD_URL}</p>", to: "$EMAIL_RECIPIENTS"

      // Slack channel to publish the notification to. Replace this url with the desired slack channel URL.
      slackSend baseUrl: 'https://cgi-global-wealth.slack.com/services/hooks/jenkins-ci/', channel: 'digital-technology', color: 'danger', message: "Build ${env.BUILD_TAG} FAILED for Jira tickets:${JIRA} at(<${env.BUILD_URL}|Jenkins Link>)", tokenCredentialId: 'slack-integration-digitaltech'
    }
  } */
}
