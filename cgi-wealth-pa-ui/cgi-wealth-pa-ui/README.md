# Wealth360 Platform Amazo Angular Boilerplate

[<img src="http://icon-icons.com/icons2/478/PNG/512/slack_47017.png" width="7%">](https://join.slack.com/t/w360-tech-platform/shared_invite/MjIwMDgwNzMxMzYyLTE1MDE1MjM4OTItYzEwMzk0NmUzZg)

The purpose of this repository is to allow developers to design and develop in a configured workspace along with best practices outlined.

## Setup

Install the following applications on your local development environment:

- [NodeJS](https://nodejs.org/en/) (Download Long Term Support LTS version)
- [Visual Studio Code](https://code.visualstudio.com/)
- [Homebrew](https://brew.sh/) (MacOS only)
- [Git](https://git-scm.com/) (Windows only)
- [GitKraken](https://www.gitkraken.com/git-client) - Git GUI Client

### Install Node packages globally

Run the following commands in Terminal(MacOS) or Command Prompt(Windows) once on your machine:

```
npm install -g @angular/cli tslint typescript

# If you are on MacOS, and you run into permission issue while trying to install, run the following command:
sudo npm install -g @angular/cli tslint typescript

# Update your dependencies globally if necessary
npm update -g @angular/cli tslint typescript
```

### Install Node packages locally

Run the following commands in Terminal(MacOS) or Command Prompt(Windows) to install node packages locally in the repository:

```
npm install

# Update node dependencies locally if necessary
npm update
```

### Recommended IDE

The recommended IDE for development is [Visual Studio Code](https://code.visualstudio.com/).

![Visual Studio Code](https://code.visualstudio.com/home/home-screenshot-mac-lg.png)

The following extensions are recommended to be installed on your Visual Studio Code to make your life as developer more effective:

- [Angular TypeScript Snippets](https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2)
- [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)
- [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)
- [Bracket Pair Colorizer](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer)
- [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
- [Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)
- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [HTML Snippets](https://marketplace.visualstudio.com/items?itemName=abusaidm.html-snippets)
- [npm](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script)
- [npm Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense)
- [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)
- [Style Formatter](https://marketplace.visualstudio.com/items?itemName=dweber019.vscode-style-formatter)
- [TSLint](https://marketplace.visualstudio.com/items?itemName=eg2.tslint)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

### Useful Chrome Extension

#### Lighthouse

[Lighthouse](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk) is an open-source, automated tool for improving the quality of web pages. It has audits for performance, accessibility, progressive web apps, and more. It generates a report on how well the page did. From there, use the failing audits as indicators on how to improve the page. Each audit has a reference doc explaining why the audit is important, as well as how to fix it.

![Lighthouse Chrome Extension](https://developers.google.com/web/tools/lighthouse/images/menu.png)

#### Axe

[Axe](https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd?utm_source=chrome-ntp-icon) is a Chrome extension that also scans for accessibility issues. It is better than Lighthouse because it is able to scan without refreshing the screen, while Lighthouse will refresh the screen. This is extremely useful if you have hidden dialog or popup screens. 

#### Augury

[Augury](https://chrome.google.com/webstore/detail/augury/elgalmkoelokbchhkhacckoklkejnhcd) is a Chrome extension that extends default Chrome Developer tools by adding capabilities to debug and profile Angular 2+ applications

---

## Angular Cli Commands

The Angular CLI is a command-line interface tool that you use to initialize, develop, scaffold, and maintain Angular applications. You can use the tool directly in a command shell. Please refer to the complete list of commands on [Angular](https://angular.io/cli) website. Below are some of the common commands you would normally run.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

### Internationalization (i18n)

Run `npm run i18n` to generate the standard translation source file, which is located under src/locale folder. By default, this boilerplate will generate a French translation file, `messages.fr.xlf`. You may add new language by modifying the xliffmerge.json. Please refer to [ngx-i18nsupport](https://github.com/martinroob/ngx-i18nsupport) to see list of options.

After the French translation source file has been generated, you have to translate that file by yourself. Use a translation tool, if you want. At the end, you just replace the target elements with the translated version and you change the state attribute to state="translated".

Run `npm run i18n` again if you have added new i18n translation messages to the app. It will help to merge previous translated message with the new messages into one file. You will see target elements with translated remained untouched, and target elements with `state="new"`.

Run `ng serve --aot --locale fr --i18n-format xlf --i18n-file src/locale/messages.fr.xlf` to load the translation file in the local environment.

Run `ng build --prod --output-path dist/fr --locale fr --i18n-format xlf --i18n-file src/locale/messages.fr.xlf` to build the application with a specific locale.

For more information on how to embed translation in the code, please refer [Angular Guide](https://angular.io/guide/i18n).

### Generating Documentation

[Compodocs](https://compodoc.github.io/) is a documentation tool for Angular applications. It generates a static documentation of your application.

![Compodocs tool](https://raw.githubusercontent.com/compodoc/compodoc/master/screenshots/main-view.gif)

[Compodocs](https://compodoc.github.io/) helps Angular developers providing a clear and helpful documentation of their application. Others developers of your team, or internet visitors for a public documentation, can easily understand the features of your application or library.

Run `npm run docs:serve` to generate documentation for all Angular objects.

In order to publish the documentation, run `npm run docs:build` and the content should be available under the documentation directory.

---

## Programming Model
A programming model defines how to structure and modularize the code, and how to implement new features and functionality. There are best practices and guidelines on what is and is not allowed within each of these programming constructs. The intention is to make programming simpler and more maintainable for future development. The programming model can also be considered the high-level design of the application.

Please refer to [Developer Guide](/projects/W360TP/repos/cgi-amazo-angular-boilerplate/browse/src/app/README.md) for more details.

---

## Best Practices/Success Criteria

Before pushing any code to the repository, make sure to complete the following tasks:

### 1. Code Linting/Formatting

Each repository should have a set of predefined code linting rules. These rules should be enforced at all times while you are coding. There are several ways that you can check to see if you have violated any rule:

1.  Run `ng lint` to check for formatting errors and best practices.
2.  [Visual Studio Code](https://code.visualstudio.com/) will also warn if there is any violations
3.  Run Prettier `npm run prettier` for consistent formatting.

### 2. Test driven development

Ensure to write unit test cases for each component in Angular application, **especially components/services/pipes that are reusable and shared across the application**. You must automate and unit-test the interface of the classes.

Run `ng test` and ensure 100% success rate.

References:

1.  [Walk-through of basic Karma/Jasmine Testing principles](https://medium.com/google-developer-experts/angular-2-testing-guide-a485b6cb1ef0)
2.  [Testing examples](http://blog.rangle.io/testing-angular-2-applications/)
3.  [Angular best practices](https://angular.io/guide/testing#test-a-component)
4.  [Consuming a REST API call in testing](https://offering.solutions/blog/articles/2016/02/01/consuming-a-rest-api-with-angular-http-service-in-typescript/)

### 3. Accessibility

Run the [Lighthouse](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk) and [Axe](https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd?utm_source=chrome-ntp-icon) on your new components/pages, and make sure you achieve 100% score on accessibility.

Lighthouse and Axe are only meant to help to identify potential issue of accessibility. However, to be fully compliant to AODA and WCAG 2.0, you will still require screen reader and keyboard to navigate the site.

For detailed guidelines for WCAG2.0, please check: https://www.w3.org/WAI/WCAG20/quickref/

### 4. Compodoc

Run `npm run docs:serve` or `npm run docs:coverage` and verify that the documentation coverage is complete for the new code changes.

![Compodoc Documentation Coverage](https://compodoc.github.io/website/assets/img/screenshots/8.png)

Please refer to [Compodoc Guides](https://compodoc.github.io/website/guides/comments.html) on how to properly document Angular classes and its members.

---

## Further help

[<img src="http://icon-icons.com/icons2/478/PNG/512/slack_47017.png" width="7%">](https://join.slack.com/t/w360-tech-platform/shared_invite/MjIwMDgwNzMxMzYyLTE1MDE1MjM4OTItYzEwMzk0NmUzZg)

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://angular.io/cli).
